FROM composer as builder
WORKDIR /app/
COPY composer.* ./
RUN composer install --no-dev

FROM registry.gitlab.com/aria-php/aria-php-container:7.4

ENV DEPLOY_DIRECTORY=${APACHE_DOCUMENT_ROOT}
ENV APACHE_DOCUMENT_ROOT=${DEPLOY_DIRECTORY}webroot/

RUN sed -ri -e "s!${DEPLOY_DIRECTORY}!${APACHE_DOCUMENT_ROOT}!g" /etc/apache2/sites-available/*.conf && \
  sed -ri -e "s!${DEPLOY_DIRECTORY}!${APACHE_DOCUMENT_ROOT}!g" /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

COPY .env ${DEPLOY_DIRECTORY}.env
COPY fae ${DEPLOY_DIRECTORY}fae
COPY app.php ${DEPLOY_DIRECTORY}app.php

COPY webroot/ ${DEPLOY_DIRECTORY}webroot/
COPY --from=builder /app/vendor ${DEPLOY_DIRECTORY}vendor
COPY frames/ ${DEPLOY_DIRECTORY}frames/