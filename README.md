# FAE API Primer

A skeleton project with all the necessary folders and bits to make a REST API

## Next steps

You will want to start by modifying the name of your first frame to something
that makes sense to you. The intention of a `frame` is to keep a single module
within your API separate from others. This gives you the opportunity to split
each frame out into a component composer module in the future should you require
the same functionality in another microservice.

A `frame` does not need to be an MVC component, and can in fact be something
that provides very different functionality, such as authentication mechanisms or
any generic component.