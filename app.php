<?php

use Symfony\Component\Dotenv\Dotenv;

$dotenv = new DotEnv();
$dotenv->loadEnv(__DIR__ . '/.env');

function configLoadEnv(string $variable, ?string $default = null)
{
  if (array_key_exists($variable, $_ENV)) {
    return $_ENV[$variable];
  }
  return $default;
}

$config = (object) [
  'name'        => configLoadEnv('NAME', 'FAE API Primer'),
  'namespace'   => configLoadEnv('NAMESPACE', 'API'),
  'approot'     => __DIR__,
  'path'        => configLoadEnv('WEBPATH', ''),
  'root'        => configLoadEnv('ROOT', 'http://localhost'),
  'timezone'    => configLoadEnv('TIMEZONE', 'UTC'),
  'salt'        => configLoadEnv('SALT', ''),
  'framePaths'  => [
    __DIR__ . '/vendor/fae',
    __DIR__ . '/frames',
  ],
  'apiVersion'  => 'v1',
  //'recaptchaPrivate' => '6LftxcgUAAAAAKSptbXE-ekPoaIplHu96GNsSGjZ',
  'public'      => (object) [
    //'recaptchaPublic' => '6LftxcgUAAAAAOhA4UJngYXUuplJEvanPadfxk53',
  ],
  'authClass'     => '\\FAE\\auth_oidc\\auth',
  'oidcProvider'  => '\\FAE\\auth_oidc\\oidc',
  'oidcSettings'  => [
    'clientId'      => configLoadEnv('OIDC_CLIENT_ID', ''),
    'clientSecret'  => configLoadEnv('OIDC_CLIENT_SECRET', ''),
    'baseurl'       => configLoadEnv('OIDC_BASEURL', ''),
    'scopes'        => configLoadEnv('OIDC_SCOPES', 'openid'),
  ],
  'graphQLSettings' => [
    'endpoint'      => configLoadEnv('GRAPHQL_SERVER', ''),
  ],
];

$dataSource = [
  'default' => [
    'dbname'    => configLoadEnv('DBNAME'),
    'user'      => configLoadEnv('DBUSER'),
    'password'  => configLoadEnv('DBPASS'),
    'host'      => configLoadEnv('DBHOST'),
    'driver'    => configLoadEnv('DBDRIVER', 'mysqli'),
  ]
];

error_reporting(configLoadEnv('DEBUG', false) ? E_ALL ^ E_NOTICE : E_ERROR);
ini_set('display_errors', configLoadEnv('DEBUG', false) ? 1 : 0);

define('APPROOT', $config->approot);
define('BASE_URL', $config->root . $config->path);
define('SALT', $config->salt);

session_start();
