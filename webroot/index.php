<?php

/**
 * FAE 
 *
 * Copyright 2021 Callum Smith
 */
$autoload = require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../app.php';

use FAE\fae\fae;
use FAE\fae\routes;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

date_default_timezone_set($config->timezone);

$schema = new \FAE\schema\model\schema();
$schema->loadSchema();

fae::loadHooks();

routes::bootstrapLoader();
$context = new RequestContext($config->path, $_SERVER['REQUEST_METHOD'], $_SERVER['HTTP_HOST'], ($_SERVER['HTTPS'] ? 'https' : 'http'));
$matcher = new UrlMatcher(routes::getCollection(), $context);

$loadUrl = preg_replace("/^" . preg_quote($config->path, '/') . "/", '', $_SERVER['REDIRECT_URL']);

try {
  $parameters = $matcher->match($loadUrl);
  if(class_exists($parameters['_controller'])){
    $class = new $parameters['_controller']();
    call_user_func($class, $parameters);
  } else {
    call_user_func($parameters['_controller'], $parameters);
  }
} catch (ResourceNotFoundException $e) {
  http_response_code(404);
  header("Content-type: application/json");
  echo '{"error_code":404,"error":"API path not found"}';
} catch(MethodNotAllowedException $e) {
  http_response_code(405);
  header("Content-type: application/json");
  echo '{"error_code":405,"error":"Method not allowed for this API"}';
}